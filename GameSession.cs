﻿using UnityEngine;
using System.Collections;

//Для каждой карты скрипт свой. Этот тестовый
public class GameSession : MonoBehaviour {

    private CameraController GUICamera;
    private PlayerINfo PInfo;

    public System.Collections.Generic.List<spawnerZ> spawners;//спавнеры зомби

    public float pauseTime = 5f;//время между волнами
    public int ZombieCount = 0;//общее число зомби в волне

    public int MaxZombies = 30;
    private bool TooManyZombies = false;
    public int CountZombiesAtMap;//кол-во зомби на карте

    public int ZombiesInWave;//Zombies - среднее кол-во зомби в каждом спавнере в последней волне
    public float SpawnTimeInWave;//SpawnTime - среднее время появления зомби в последней волне

    public bool waveActive = false;
    public int waveNumber = 1;


	void Start () 
    {
        GUICamera = GameObject.Find("Main Camera").GetComponent<CameraController>();
        PInfo = GameObject.Find("Player").GetComponent<PlayerINfo>();
	}
	
	void Update () 
    {
        if (!waveActive)//Запуск новых волн
        {
            waveActive = true;
            ZombiesInWave += 3;//магические числа!!!
            if (SpawnTimeInWave > 2) SpawnTimeInWave -= 0.5f;//магические числа!!!

            StartCoroutine(wave(ZombiesInWave, SpawnTimeInWave));
        }

        //Приостанавливает спавн зомби, если тех много на карте
        if (CountZombiesAtMap >= MaxZombies)
        {
            TooManyZombies = true;
            foreach (spawnerZ SZ in spawners)
            {
                if (SZ.Active)
                {
                    SZ.pause = true;
                }
            }
        }
        else if (TooManyZombies)
        {
            TooManyZombies = false;
            foreach (spawnerZ SZ in spawners)
            {
                if (SZ.Active)
                {
                    SZ.pause = false;
                }
            }
        }
	}

    public void KillZombie()
    {
        PInfo.money += 10;
        ZombieCount--;
    }

    //Zombies - среднее кол-во зомби в каждом спавнере
    //SpawnTime - среднее время появления зомби
    //Метод настраивает спавнеры -- ждет, пока будут уничтожены все зомби -- ждет, пока закончится время между волнами
    private IEnumerator wave(int Zombies, float SpawnTime)
    {
        foreach (spawnerZ SZ in spawners)
        {
            if (SZ.Active)
            {
                int CountZ = (int)Random.Range(Zombies - 3, Zombies + 3);//магические числа!!!
                SZ.maxZombies = CountZ;
                ZombieCount += CountZ;
                SZ.time = Random.Range(SpawnTime - 3f, SpawnTime + 3f);//магические числа!!!
                SZ.ZombieCount = 0;
            }
        }

        while (ZombieCount > 0)
        {
            yield return null;
        }
        //восстановление игрока после уничтожения волны
        PInfo.energy = PInfo.MaxEnergy;
        PInfo.HP = PInfo.maxHP;
        //-----
        yield return new WaitForSeconds(pauseTime);
        waveActive = false;
        waveNumber++;
        yield break;
    }

}
