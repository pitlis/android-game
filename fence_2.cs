﻿using UnityEngine;
using System.Collections;

public class fence_2 : MonoBehaviour {

    public Sprite myButtomTexture;

    public float HP;
    public float HPmax = 30f;

    public float timeRepair;//задержка ремонта
    private float myTimerRepair;
    public bool isActive = true;

    

    void Start()
    {
        myTimerRepair = 0;
        HP = HPmax;
    }

    void Update()
    {
        if (myTimerRepair > 0)
        {
            myTimerRepair -= Time.deltaTime;
        }

        if (HP >= 20)
            this.renderer.material.color = Color.green;
        else if (HP >= 10)
            this.renderer.material.color = Color.yellow;
        else if (HP > 0)
            this.renderer.material.color = Color.red;

        if (HP <= 0)
        {
            this.renderer.material.color = Color.black;
            isActive = false;
            GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Zombie" && isActive)
        {
            collider.gameObject.GetComponent<Zombie>().CanNotGo = collider.gameObject.transform.position.x < this.transform.position.x ? 1 : -1;
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Zombie")
        {
                collider.gameObject.GetComponent<Zombie>().CanNotGo = 0;
        }
    }


    public bool Hurt(int damage)
    {
        HP -= damage;
        if (HP <= 0)
            return true;
        return false;
    }

    public void repair()
    {
        if (myTimerRepair <= 0)
        {
            myTimerRepair = timeRepair;
            if (HP < HPmax)
                HP += 3;
            if (!isActive)
            {
                isActive = true;
            }
        }

    }
}
