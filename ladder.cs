﻿using UnityEngine;
using System.Collections;

public class ladder : MonoBehaviour {

    public Sprite myButtomTexture;

    Transform sister;
    public bool PlayerInTrigger = false;

    private Collider2D Player;

    public bool move = false;//есть ли метод у игрока


    public float timeLadder;//задержка перемещения игрока
    public float myTimerLadder;

    void Update()
    {
        if (myTimerLadder > 0)
        {
            myTimerLadder -= Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        if (myTimerLadder <= 0)
        {
            if (PlayerInTrigger && !move)
            {
                move = true;
                Player.gameObject.GetComponent<PlayerController>().MoveButtonTexture = myButtomTexture;
                Player.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                Player.gameObject.GetComponent<PlayerController>().Move += () => { teleport(Player); };
            }
        }

        if (!PlayerInTrigger && move)
        {
            move = false;
            Player.gameObject.GetComponent<PlayerController>().MoveButtonTexture = Player.gameObject.GetComponent<PlayerController>().BaseButtonTexture;
            Player.gameObject.GetComponent<PlayerController>().ChangTexture = true;
            Player.gameObject.GetComponent<PlayerController>().Move = null;
        }
    }

    void Start()
    {
        myTimerLadder = 0;
        if (this.name == "switchtop1")
            sister = this.transform.parent.FindChild("switchtop2");
        else
            sister = this.transform.parent.FindChild("switchtop1");
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            Player = collider;
            PlayerInTrigger = true;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
                Player = collider;
                PlayerInTrigger = false;
        }
    }

    public void teleport(Collider2D collider)
    {
        if (collider.gameObject.GetComponent<PlayerController>() != null)//если переместился игрок - запускаем таймер
        {
            myTimerLadder = timeLadder;
            sister.GetComponent<ladder>().myTimerLadder = timeLadder;
        }
        collider.gameObject.transform.position = sister.transform.position;
    }
}
