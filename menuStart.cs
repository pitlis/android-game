﻿using UnityEngine;
using System.Collections;

public class menuStart : MonoBehaviour {

    public Rect playGameRect;
    public GUISkin GUIObject;

    void Awake()
    {
        //DontDestroyOnLoad(this);
    }
    
    public void OnGUI()
    {
            GUI.skin = GUIObject;
            if (GUI.Button(playGameRect, "Play Game"))
            {
                Application.LoadLevel("Game");
            }
    }
}
