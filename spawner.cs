﻿using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour {

    private bool IamFree = false;
    public System.Collections.Generic.List<GameObject> myObjects;//список объектов, которые может спавнить. Заполняется перетаскиванием префабов в редакторе
    private GameObject myObject;

    public float time;//время до появления нового объекта
    public float myTimer;

    void Update()
    {
        if (myObject == null && IamFree == false)
        {
            IamFree = true;
            myTimer = time;
            GetComponent<SpriteRenderer>().enabled = true;
        }

        if (myTimer > 0)
        {
            myTimer -= Time.deltaTime;
        }
        else if (IamFree)
        {
            spawn();
        }

    }
    

    void spawn()
    {
        int random = (int)Random.Range((float)0, (float)myObjects.Count);
        myObject = (GameObject)Instantiate(myObjects[random]);
        myObject.transform.position = this.transform.position;
        GetComponent<SpriteRenderer>().enabled = false;
        IamFree = false;
    }






}
