﻿using UnityEngine;
using System.Collections;

public class isWeapon : MonoBehaviour {


    public Sprite myButtomTexture;

    public GameObject wp; //оружие
    public int amm;
    public int price = 0;
    public int maxAmmunition;//макс кол-во патронов этого оружия, которое может носить игрок

    public float timeBuy = 0.5f;//задержка между покупками
    public float myTimerBuy;

    public bool move = false;

    PlayerINfo PInfo;

    void Start()
    {
        myTimerBuy = -1;
        PInfo = GameObject.Find("Player").GetComponent<PlayerINfo>();
    }

    void Update()
    {
        if (myTimerBuy > 0)
        {
            myTimerBuy -= Time.deltaTime;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (!move)
            {
                move = true;
                collider.gameObject.GetComponent<PlayerController>().MoveButtonTexture = myButtomTexture;
                collider.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                collider.gameObject.GetComponent<PlayerController>().Move += () => { buy(); };
            }
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (move)
            {
                move = false;
                collider.gameObject.GetComponent<PlayerController>().MoveButtonTexture = collider.gameObject.GetComponent<PlayerController>().BaseButtonTexture;
                collider.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                collider.gameObject.GetComponent<PlayerController>().Move = null;
            }
        }
    }
    
    public void buy()
    {
        if (myTimerBuy <= 0)
        {
            myTimerBuy = timeBuy;
            if (PInfo.money > price)
            {
                PInfo.money -= price;
                if (lookForWeapon() != 0)//если это оружие уже есть - добавление патронов
                {
                    int n = lookForWeapon();
                    if (!AddAmmunition(n))
                        PInfo.money += price;
                    return;
                }
                //если есть свободное место под оружие - пихаю в него
                if (PInfo.arsenal[1].weapon == null)
                {
                    AddWeapon(1);
                    return;
                }
                if (PInfo.arsenal[2].weapon == null)
                {
                    AddWeapon(2);
                    return;
                }
                //----

                //места нет - пихаю вместо текущего (если оно не базовое)
                if (PInfo.NumberWeaponActive != 0)
                    AddWeapon(PInfo.NumberWeaponActive);
                return;
            }
        }
    }

    int lookForWeapon()
    {
        if (wp.GetComponent<turretForPlayer>() != null && PInfo.arsenal[1].weapon != null && PInfo.arsenal[1].weapon.GetComponent<turretForPlayer>() != null) return 1;
        if (wp.GetComponent<turretForPlayer>() != null && PInfo.arsenal[2].weapon != null && PInfo.arsenal[2].weapon.GetComponent<turretForPlayer>() != null) return 2;
        if (wp.GetComponent<turretForPlayer>() != null) return 0;

        if (PInfo.arsenal[1].weapon != null && PInfo.arsenal[1].weapon.GetComponent<Weapon1>() != null && PInfo.arsenal[1].weapon.GetComponent<Weapon1>().myName == wp.GetComponent<Weapon1>().myName) return 1;
        if (PInfo.arsenal[2].weapon != null && PInfo.arsenal[2].weapon.GetComponent<Weapon1>() != null && PInfo.arsenal[2].weapon.GetComponent<Weapon1>().myName == wp.GetComponent<Weapon1>().myName) return 2;
        return 0;
    }
    void AddWeapon(int number)
    {
        PInfo.arsenal[number].weapon = wp;
        if (number == PInfo.NumberWeaponActive)
        {
            PInfo.SwitchWeapons(number);
            PInfo.ammunition = amm;
        }
        PInfo.arsenal[number].ammunition = amm;
    }
    bool AddAmmunition(int number)
    {
        //если нет места для патронов
        if (
            (PInfo.NumberWeaponActive == number && PInfo.ammunition >= maxAmmunition) ||
            (PInfo.NumberWeaponActive != number && PInfo.arsenal[number].ammunition >= maxAmmunition)
            )
        {
            return false;
        }

        //место для патронов есть
        if (PInfo.NumberWeaponActive == number)
            PInfo.ammunition += amm;
        else
            PInfo.arsenal[number].ammunition += amm;

        //проверка и удаление лишних патронов
        if (PInfo.NumberWeaponActive == number && PInfo.ammunition >= maxAmmunition)
            PInfo.ammunition = maxAmmunition;
        if (PInfo.NumberWeaponActive != number && PInfo.arsenal[number].ammunition >= maxAmmunition)
            PInfo.arsenal[number].ammunition = maxAmmunition;

        return true;
    }

}
