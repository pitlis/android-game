﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    //Компоненты для управления со смартфона. В редакторе сюда цепляем джойстик и кнопки.
    public CNJoystick movementJoystick;
    public CNButton B1;
    public CNButton B2;
    public CNButton Bleft;
    public CNButton Bright;
    //----

    //Временные параметры для отладки
    public float playerSpeed;  //скорость
    private float StartRunning = 0;
    //----

    //Анимация
    private Animator anim;
    public float isRun;
    public int direction = 1; //текущее направление player: -1 - лево, 1 - право.
    //----

    //Для обработки прыжка
    public bool onPlatform = false;
    private Transform groundCheck; // Заземление
    //----

    PlayerINfo PInfo;//информация об игроке
    public int CanNotGo = 0;//сторона, в которую в данный момент запрещено двигаться игроку. 0 - ничего не запрещено, -1 - лево, 1 - право.

    //Для обработки действий
    public delegate void MoveDelegate();
    public MoveDelegate Move;

    public delegate void ActiveButton();
    public event ActiveButton AB1;
    public event ActiveButton AB2;
    //----

    private GUICamera GCamera;
    
    public Sprite BaseButtonTexture;
    public Sprite MoveButtonTexture;
    public bool ChangTexture = true;//true, если пора сменить текстуру на кнопке действия

    void Awake()
    {
        //Для стандартных кнопок
        GCamera = GameObject.Find("GUI Camera").GetComponent<GUICamera>();
        GCamera.WeaponButton += () => { SwitchWeapons(PInfo.NumberWeaponActive); };
        //----

        //Управление со сматрфона
        Bleft.FingerTouchedEvent += () => { StartRunning = -1; };
        Bleft.FingerLiftedEvent += () => { StartRunning = 0; };
        Bright.FingerTouchedEvent += () => { StartRunning = 1; };
        Bright.FingerLiftedEvent += () => { StartRunning = 0; };

        /*
        movementJoystick.FingerLiftedEvent += () => { StartRunning = 0; };
        movementJoystick.JoystickMovedEvent += (relativeVector) =>
        {
            if (relativeVector.x > 0.5f)
                StartRunning = 1;
            else
                if (relativeVector.x < -0.5f)
                    StartRunning = -1;
                else
                    StartRunning = 0;
        };
        */
        //Раскомментировать при сборке для смартфона
        /*
        B1.FingerTouchedEvent += () => { AB1 += () => shoot(); };
        B1.FingerLiftedEvent += () => { AB1 = null; };
        B2.FingerTouchedEvent += () => { AB2 += () => move(); };
        B2.FingerLiftedEvent += () => { AB2 = null; };
        */
        //-----
        //Закомментировать при сборке для смартфона
        
        B1.FingerTouchedEvent += () => { shoot(); };
        B2.FingerTouchedEvent += () => { move(); }; 
		
        //----

    }

    void Start()
    {
        anim = this.GetComponent<Animator>();
        groundCheck = transform.Find("GroundCheck"); // Находит объект с именем "GroundCheck"
        PInfo = this.GetComponent<PlayerINfo>();
        MoveButtonTexture = BaseButtonTexture;
        //sh = GetComponentInChildren<GameObject>();        
    }

    void Update()
    {
        onPlatform = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Platform")); // Проверяет, находится ли наш объект на земле
        Animation();
        if (ChangTexture)
        {
            ChangTexture = false;
            B2.ChangeTexture(MoveButtonTexture);
        }
    }

    void FixedUpdate()
    {
        //Обработка нажатий кнопок
        if (AB1 != null)
            AB1();
        if (AB2 != null)
            AB2();
        //----
        if (PInfo.isEnabled) // проверка условия возможности управления
        {
            //Управление с клавиатуры
            float x;
            if ((x = Input.GetAxis("Horizontal")) != 0)
                run(x);
            else isRun = 0;

            if (Input.GetAxis("Jump") > 0)//пробел - действие
                //jump();
            if (Input.GetButtonDown("Fire1"))
                shoot();
            //----

            //обработка управления с джойстика
            if (StartRunning != 0f)
            {
                run(StartRunning);
            }
        }

    }



    int move()
    {
        if (Move != null)
            Move();
        return 0;
    }


    void run(float X)
    {
        isRun = X;
        if (CanNotGo != direction)
        {
            float transH = X * playerSpeed * Time.deltaTime;
            transform.Translate(new Vector2(transH, 0));
        }
    }

    void shoot()
    {
        if (PInfo.weaponActive.GetComponent<turretForPlayer>() != null && PInfo.ammunition >= 1)
        {
            if (PInfo.weaponActive.GetComponent<turretForPlayer>().install(this.transform.position))
            {
                PInfo.ammunition--;
            }
            else
            {
                //вывод сообщения о том, что турель здесь установить невозможно
            }
            return;
        }

        if (PInfo.isEnabled) // На всякий случай.
        {
            //если выстрел из базового оружия
            if (PInfo.NumberWeaponActive == 0 && PInfo.energy >= 1)
            {
                if (PInfo.weaponActive.GetComponent<Weapon1>().shoot(direction))
                    PInfo.energy--;
                return;
            }
            if (PInfo.ammunition >= 1)
            {
                if (PInfo.weaponActive.GetComponent<Weapon1>().shoot(direction))
                    PInfo.ammunition--;
            }
        }
    }

    void SwitchWeapons(int numberWeaponActive)
    {
        int number;
        if (numberWeaponActive + 1 > 2) number = 0;
        else number = numberWeaponActive + 1;

        if (PInfo.arsenal[number].weapon != null)
            PInfo.SwitchWeapons(number);
        else
            SwitchWeapons(number+1);
    }

    //АНИМАЦИЯ
    void Animation() //Обработка анимации
    {
        if (onPlatform)
            anim.SetBool("Jump", false);
        else
            anim.SetBool("Jump", true);


        if (isRun > 0)
        {
            if (direction == -1) Flip();
            anim.SetBool("Right", true);
            direction = 1;
        }
        else
        {
            if (isRun < 0)
            {
                if (direction == 1) Flip();
                anim.SetBool("Right", true);
                direction = -1;
            }
            else
            {
                anim.SetBool("Right", false);
            }
        }

    }

    void Flip() //Поворот
    {
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    //----



}
