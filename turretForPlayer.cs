﻿using UnityEngine;
using System.Collections;

public class turretForPlayer : MonoBehaviour {

    public GameObject turret;

    public bool install(Vector2 pos)
    {
        Object.Instantiate(turret, pos, new Quaternion());
        return true;
    }


}
