﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public Transform target;

    private float BorderLeft;
    private float BorderRight;

    public int unit;//кол-во пикселей в одном юните
    private float Xwidth;//половина ширины камеры в юнитах
    public float OrthographicBase = 6.466817f;

    public float HEigh;

    void Start()
    {
        unit = (int)((this.camera.pixelHeight / 2)/this.camera.orthographicSize);
        Xwidth = camera.pixelWidth / 2 / unit;
        HEigh = this.camera.pixelHeight;
        
        if (unit != 34)//магическое число!!!
        {
            //а тут вообще магия...будет ли это работать для разных разрешений? загадка
            this.camera.orthographicSize = this.camera.orthographicSize + 22.6f / this.camera.pixelHeight * this.camera.orthographicSize;
            //---
            unit = (int)((this.camera.pixelHeight / 2) / this.camera.orthographicSize);
            Xwidth = camera.pixelWidth / 2 / unit;
        }
        
        BorderLeft = GameObject.Find("BorderCamera/Left").transform.position.x;
        BorderRight = GameObject.Find("BorderCamera/Right").transform.position.x;
    }

	void Update() 
    {
        if (changePosition())
            transform.position = new Vector3(0,0,-10) + target.position;
    }


    bool changePosition()
    {
        float Xnew = target.position.x;
        float X = this.transform.position.x;

        if (Xnew > X)
        {
            if (X + Xwidth > BorderRight)
                return false;
            else return true;
        }
        if (Xnew < X)
        {
            if (X - Xwidth < BorderLeft)
                return false;
            else return true;
        }
        return true;
    }



}
