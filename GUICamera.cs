﻿using UnityEngine;
using System.Collections;

public class GUICamera : MonoBehaviour {

    public Transform target;
    private PlayerINfo PI;
    private PlayerController PC;
    private GameSession GS;

    public delegate void OnButton();
    public event OnButton WeaponButton;

    void Start()
    {
        CameraInicialize();

        PI = target.GetComponentInChildren<PlayerINfo>();
        PC = target.GetComponentInChildren<PlayerController>();
        GS = GameObject.Find("GameSession").GetComponentInChildren<GameSession>();
    }

    void Update()
    {
        transform.position = new Vector3(0, 0, -10) + target.position;
    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 200), "Патроны: " + PI.ammunition.ToString());
        GUI.Label(new Rect(10, 30, 200, 100), "Здоровье: " + PI.HP.ToString());
        GUI.Label(new Rect(10, 50, 200, 100), "Энергия: " + PI.energy.ToString());
        GUI.Label(new Rect(10, 80, 200, 100), "Деньги: " + PI.money.ToString());
        GUI.Label(new Rect(10, 110, 200, 100), "Волна: " + GS.waveNumber.ToString());

        if (GUI.Button(new Rect(Screen.width - 90, 30, 80, 30), "Оружие"))
            if (WeaponButton != null)
                WeaponButton();
    }

    private float BorderLeft;
    private float BorderRight;

    public int unit;//кол-во пикселей в одном юните
    private float Xwidth;//половина ширины камеры в юнитах

    public float HEigh;

    void CameraInicialize()
    {
        unit = (int)((this.camera.pixelHeight / 2) / this.camera.orthographicSize);
        Xwidth = camera.pixelWidth / 2 / unit;
        HEigh = this.camera.pixelHeight;

        if (unit != 34)//магическое число!!!
        {
            //а тут вообще магия...будет ли это работать для разных разрешений? загадка
            this.camera.orthographicSize = this.camera.orthographicSize + 22.6f / this.camera.pixelHeight * this.camera.orthographicSize;
            //---
            unit = (int)((this.camera.pixelHeight / 2) / this.camera.orthographicSize);
            Xwidth = camera.pixelWidth / 2 / unit;
        }

        BorderLeft = GameObject.Find("BorderCamera/Left").transform.position.x;
        BorderRight = GameObject.Find("BorderCamera/Right").transform.position.x;
    }

    bool changePosition()
    {
        float Xnew = target.position.x;
        float X = this.transform.position.x;

        if (Xnew > X)
        {
            if (X + Xwidth > BorderRight)
                return false;
            else return true;
        }
        if (Xnew < X)
        {
            if (X - Xwidth < BorderLeft)
                return false;
            else return true;
        }
        return true;
    }
}
