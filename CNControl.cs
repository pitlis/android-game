﻿/**
 * modified by @moscoquera
 * 
 * */


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Placement snap enum
public enum PlacementSnap
{
    leftTop,
    leftBottom,
    rightTop,
    rightBottom,
	Fixed
}

public enum TouchDetectionType{
	area,
	radius
}
/**
 * Joystic move event delegate. 
 *  Magnitude is a value from 0 to 1 inclusive.
 *  0 is Center and 1 is full radius
 */
public delegate void JoystickMoveEventHandler(Vector3 relativeVector);
public delegate void FingerLiftedEventHandler();
public delegate void FingerTouchedEventHandler();

public abstract class CNControl : MonoBehaviour
{
    // protected delegate to automatically switch between Mouse input and Touch input
    protected delegate void InputHandler();

    public bool remoteTesting = false;
    // Editor variables
    //public float pixelToUnits = 1000f;
    public PlacementSnap placementSnap = PlacementSnap.leftBottom;
	public Rect tapZone;
	public TouchDetectionType DetectionType=TouchDetectionType.area;
	// Radius of the joystick movement
	public float joystickMovementRadius = 3f;


    // Script-only public variables
    public Camera CurrentCamera { get; set; }
    /**
     * protected instance variables
     */
    
    // Camera frustum height
    protected float frustumHeight;
    // Camera frustum width
    protected float frustumWidth;
    // Finger ID to track
    protected int myFingerId = -1;
    // Where did we touch initially
    protected Vector3 invokeTouchPosition;
    // Relative position of the small joystick circle
    protected Vector3 joystickRelativePosition;
    // Screen point in units cacher variable
    protected Vector3 screenPointInUnits;
    // Magic Vector3, needed for different snap placements
    protected Vector3 relativeExtentSummand;
    // This joystick is currently being tweaked
    protected bool isTweaking = false;
    // Touch or Click
    protected InputHandler CurrentInputHandler;
    // Distance to camera
    protected float distanceToCamera = 0.5f;
    // Half of screen sizes
    protected float halfScreenHeight;
    protected float halfScreenWidth;
    // Full screen sizes
    protected float screenHeight;
    protected float screenWidth;
    // Snap position, relative joystick position
    protected Vector3 snapPosition;
    // (-halfScreenWidth, -halfScreenHeight, 0f)
    // Visually, it's the bottom left point in local units
    protected Vector3 cleanPosition;
    // Some transform cache variables
   
    protected Transform transformCache;

	protected Vector2 fixedStepSize = new Vector2(10,10);

	protected Vector3 halfPoint;
    // Use this for initialization
    void Awake()
    {
    }

    void Update()
    {
        // Automatically call proper input handler
    }

    /** Our touch Input handler
     * Most of the work is done in this function
     */
    void TouchInputHandler()
    {
        
    }
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE
    // Mouse input handler, nothing really interesting
    // It's pretty straightforward
    void MouseInputHandler()
    {
        
    }
#endif
    /**
     * Snappings calculation
     * Joystick radius calculation based on specified pixel to units value
     * Initial on-screen placement, relative to the specified camera
     */
    void InitialCalculations()
    {
        halfScreenHeight = CurrentCamera.orthographicSize;
        halfScreenWidth = halfScreenHeight * CurrentCamera.aspect;
		screenHeight = halfScreenHeight * 2f;
        screenWidth = halfScreenWidth * 2f;
		halfPoint = new Vector3(halfScreenWidth,halfScreenHeight,0);
        snapPosition = new Vector3();
        cleanPosition = new Vector3(-halfScreenWidth, -halfScreenHeight);

        switch (placementSnap)
        {
            // We do nothing, it's a default position
            case PlacementSnap.leftBottom:
                snapPosition.x = -halfScreenWidth + tapZone.width / 2f - tapZone.x;
                snapPosition.y = -halfScreenHeight + tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = 0f;
                tapZone.y = 0f;
                break;
            // We swap Y component
            case PlacementSnap.leftTop:
                snapPosition.x = -halfScreenWidth + tapZone.width / 2f - tapZone.x;
                snapPosition.y = halfScreenHeight - tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = 0f;
                tapZone.y = screenHeight - tapZone.height;
                break;
            // We swap X component
            case PlacementSnap.rightBottom:
                snapPosition.x = halfScreenWidth - tapZone.width / 2f - tapZone.x;
                snapPosition.y = -halfScreenHeight + tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = screenWidth - tapZone.width;
                tapZone.y = 0f;
                break;
            // We swap both X and Y component
            case PlacementSnap.rightTop:
                snapPosition.x = halfScreenWidth - tapZone.width / 2f - tapZone.x;
                snapPosition.y = halfScreenHeight - tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = screenWidth - tapZone.width;
                tapZone.y = screenHeight - tapZone.height;
                break;
			case PlacementSnap.Fixed:
				snapPosition.x = (tapZone.x/fixedStepSize.x)*halfScreenWidth;
				snapPosition.y = (tapZone.y / fixedStepSize.y)*halfScreenHeight;
				
				// Tap zone change so we can utilize Rect's .Contains() method
			tapZone.x = halfScreenWidth+((tapZone.x/fixedStepSize.x)*halfScreenWidth) - (tapZone.width/2);
			tapZone.y = halfScreenHeight+((tapZone.y / fixedStepSize.y)*halfScreenHeight) - (tapZone.height/2);
			break;
				
        }
        transformCache.localPosition = snapPosition;
		 
    }

    /**
     * Touch or mouse click occured
     * Store initial local position
     * Vector3 touchPosition is in Screen coordinates
     * 
     * Returns true if finger found
     * Returns fals if not
     */
    bool TouchOccured(Vector3 touchPosition)
    {
        
        return false;
    }

    /**
     * Try to drag small joystick knob to it's desired position (in Screen coordinates)
     */
    void TweakJoystick(Vector3 desiredPosition)
    {
        
    }

    /**
     * Resetting BOTH joystick sprites to their initial position
     */
    void ResetJoystickPosition()
    {
     
    }

    /**
     * We need to convert our touch or mouse position to our local joystick position
     */
    void ScreenPointToRelativeFrustumPoint(Vector3 point)
    {
  
    }

    // Sometimes when user lifts his finger, current touch index changes.
    // To keep track of our finger, we need to know which finger has the user lifted
    int FindMyFingerId()
    {
		return -1;
    }

    void OnDrawGizmos()
	{Gizmos.color = Color.red;

		if (DetectionType==TouchDetectionType.area){
	        Vector3 gizmoPosition = new Vector3(transform.position.x + tapZone.x, transform.position.y + tapZone.y, transform.position.z);
	        Gizmos.DrawWireCube(gizmoPosition, new Vector3(tapZone.width, tapZone.height));
		}else if (DetectionType==TouchDetectionType.radius){
			Vector3 gizmoPosition = new Vector3(transform.position.x + tapZone.x, transform.position.y + tapZone.y, transform.position.z);
			Gizmos.DrawWireSphere(gizmoPosition,joystickMovementRadius);
		}
	}



}
