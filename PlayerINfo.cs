﻿using UnityEngine;
using System.Collections;

public class PlayerINfo : MonoBehaviour {

    public int maxHP = 100;
    public int HP;
    public int MaxEnergy;
    public int energy;

    public bool IsAlive = true;
    private Vector2 startPosition; // точка спавна при возрождении
    public bool isEnabled; // Доступ к управлению персонажем
    public int money = 0;

    //Оружие
    public GameObject weaponActive; //активное оружие
    public int ammunition;//патроны в активном оружии
    public GameObject weaponStart; //базовое оружие
    public int NumberWeaponActive = 0;//0 - базовое оружие, 1 - первое, 2 - второе
    public struct myWeapon
    {
        public GameObject weapon;
        public int ammunition;
    }
    public myWeapon[] arsenal = new myWeapon[3];
    //----

    void Start()
    {
        isEnabled = true;
        startPosition = this.transform.position;
        ammunition = 9999;
        weaponActive = (GameObject)Instantiate(weaponStart);
        weaponActive.transform.parent = this.transform;
        weaponActive.transform.position = this.transform.position;

        energy = MaxEnergy;
        arsenal[0].weapon = weaponStart;
        arsenal[0].ammunition = 9999;
    }

    public float timeEnergy;//задержка восстановления энергии
    private float myTimerEnergy;

    void Update()
    {
        if (myTimerEnergy > 0)
        {
            myTimerEnergy -= Time.deltaTime;
        }
        else
        {
            myTimerEnergy = timeEnergy;
            if (energy < MaxEnergy)
                energy++;
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "health")
        {           
            HP += 10;
            Destroy(collider.gameObject);
        }
    }

    public void SwitchWeapons(int number)//номер оружия, на которое меняем
    {
        arsenal[NumberWeaponActive].ammunition = ammunition;
        Destroy(weaponActive);
        weaponActive = (GameObject)Instantiate(arsenal[number].weapon);
        if (this.transform.localScale.x < 0)
            Flip();
        weaponActive.transform.parent = this.transform;
        weaponActive.transform.position = this.transform.position;
        ammunition = arsenal[number].ammunition;
        NumberWeaponActive = number;
    }

    void Flip() //Поворот
    {
        Vector3 theScale = weaponActive.transform.localScale;
        theScale.x *= -1;
        weaponActive.transform.localScale = theScale;
    }

    public void Hurt(int damage)
    {
        if (energy >= damage)
            energy -= damage;
        else
        {
            HP -= (damage - energy);
            energy = 0;
        }

        if (HP <= 0)
        {
            StartCoroutine(spawnCoroutine(2f));
        }
    }

    IEnumerator spawnCoroutine(float time)
    {
        Kill();
        yield return new WaitForSeconds(time); // Ждем заданое время
        //Revive();

        //прототип менюшки
        Application.LoadLevel("menuStart");
        //----
    }

    private void Kill()
    {
        isEnabled = false; // Блокируем управление
        this.tag = "deadPlayer";  // чтобы турелька не стреляла
        this.renderer.material.color = Color.red;
        Destroy(weaponActive);
        
    }

   /* private void Revive()
    {

        HP = 20f;
        this.transform.position = startPosition;
        this.tag = "Player";
        this.renderer.material.color = Color.white;
        OutOfAmmo();  // установка начального оружия
        isEnabled = true;
    }*/
}
