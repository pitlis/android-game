﻿using UnityEngine;
using System.Collections;

public class door : MonoBehaviour {

    public System.Collections.Generic.List<spawnerZ> spawners;//спавнеры, находящиеся за этой дверью
    public int price = 0;
    public bool move = false;
    PlayerINfo PInfo;


    public Sprite myButtomTexture;

    void Start()
    {
        PInfo = GameObject.Find("Player").GetComponent<PlayerINfo>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (!move)
            {
                move = true;
                collider.gameObject.GetComponent<PlayerController>().MoveButtonTexture = myButtomTexture;
                collider.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                collider.gameObject.GetComponent<PlayerController>().Move += () => { open(); };
            }
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (move)
            {
                move = false;
                collider.gameObject.GetComponent<PlayerController>().MoveButtonTexture = collider.gameObject.GetComponent<PlayerController>().BaseButtonTexture;
                collider.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                collider.gameObject.GetComponent<PlayerController>().Move = null;
            }
        }
    }

    public void open()
    {
        if (PInfo.money >= price)
        {
            PInfo.money -= price;
            foreach (spawnerZ sZ in spawners)
            {
                sZ.Active = true;
            }
            Destroy(gameObject);
        }
        else
        {
            ;
        }
    }

}
