﻿using UnityEngine;
using System.Collections;

public class fenceForPlayer : MonoBehaviour {

    //Для ремонта

    public bool move = false;
    private fence parent;

    void Start()
    {
        parent = this.transform.parent.gameObject.GetComponent<fence>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (!move)
            {
                move = true;
                collider.gameObject.GetComponent<PlayerController>().MoveButtonTexture = parent.myButtomTexture;
                collider.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                collider.gameObject.GetComponent<PlayerController>().Move += () => { parent.repair(); };
                collider.gameObject.GetComponent<PlayerController>().CanNotGo = parent.direction;
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (move)
            {
                move = false;
                collider.gameObject.GetComponent<PlayerController>().MoveButtonTexture = collider.gameObject.GetComponent<PlayerController>().BaseButtonTexture;
                collider.gameObject.GetComponent<PlayerController>().ChangTexture = true;
                collider.gameObject.GetComponent<PlayerController>().Move = null;
                collider.gameObject.GetComponent<PlayerController>().CanNotGo = 0;
            }
        }
    }
}
