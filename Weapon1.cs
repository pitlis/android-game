﻿using UnityEngine;
using System.Collections;

public class Weapon1 : MonoBehaviour {

    //Скрипт вешается на оружие. Для каждого оружия - свой скрипт (или свои значения переменных этого скрипта, надо еще смотреть)

    public GameObject explosion; //для префаба взрыва
    public Rigidbody2D projectile;//снаряд
    public int damage;//урон
    public float speed;
    public string myName;
    public float reloading;

    private float myTimerReloader;

    void Start () {
        ShootProjectile s = projectile.GetComponent<ShootProjectile>();
        s.myDamage = damage;
        s.myExplosion = explosion;
        myTimerReloader = 0;
	}

    void Update()
    {
        if (myTimerReloader > 0)
        {
            myTimerReloader -= Time.deltaTime;
        }
    }

    public bool shoot(int direction)
    {
        if (myTimerReloader <= 0)
        {
            myTimerReloader = reloading;
            float d; //угол поворота ракеты
            if (direction == 1)
                d = 0f;
            else d = 180f;

            Rigidbody2D instalitedProjectile = (Rigidbody2D)Instantiate(projectile, transform.position, Quaternion.Euler(0f, 0f, d));
            instalitedProjectile.GetComponent<ShootProjectile>().myParent = this.transform.parent;
            instalitedProjectile.velocity = new Vector2(direction * speed, 0);
            return true;
        }
        else return false;
    }



}
