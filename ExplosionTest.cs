﻿using UnityEngine;
using System.Collections;

public class ExplosionTest : MonoBehaviour {

    public float destroyTime;

	void Start () {
        Destroy(gameObject, destroyTime);
	}

}
