﻿using UnityEngine;
using System.Collections;

public class TargetTest : MonoBehaviour {

    public int HP = 5;//здоровье
    public GameObject healthBar;//префаб полоски здоровья

	// Use this for initialization
	void Start () {
		healthBarInitialize();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (HP <= 0)
            Destroy(gameObject);
	}


    public void Hurt(int damage)
    {
        HP-=damage;
        healthBarEdit(damage);
    }



    private void healthBarInitialize()//инициализация полоски здоровья
    {
        healthBar = (GameObject)Instantiate(healthBar);
        healthBar.transform.parent = this.transform;
        Vector2 pos;
        if (this.tag == "turret")
        {
            pos = new Vector2(this.transform.position.x*0.95f, this.transform.position.y + this.transform.localScale.y*0.7f);
        }
        else
        {
            pos = new Vector2(this.transform.position.x, this.transform.position.y + this.transform.localScale.y * 1.5f);
        }
        healthBar.transform.position = pos;
    }
    
    private void healthBarEdit(int damage)//изменение размеров полоски здоровья
    {
        float deltaHP = HP / (HP + (float)damage);
        Vector2 scale = new Vector2(healthBar.transform.localScale.x * deltaHP, healthBar.transform.localScale.y);
        healthBar.transform.localScale = scale;
    }

}
