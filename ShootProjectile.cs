﻿using UnityEngine;
using System.Collections;

public class ShootProjectile : MonoBehaviour {

    //Скрипт вешается на все снаряды. Значения урона получает из скрипта оружия.

    public GameObject myExplosion; //для префаба взрыва
    public int myDamage;
    public Transform myParent;
    public string nameWeapon;
    private bool explosion = false; //true, когда обрабатывается взрыв ракеты

    private Collider2D coll;//объект, в который попали

	void Start () {
        Destroy(gameObject, 1f);
	}
    void OnTriggerEnter2D(Collider2D collider)
    {
        //обработка взрыва ракеты, выполняется в последнюю очередь, после нижних условий
        if (explosion)
        {
            flareGunAttack(collider);
            return;
        }
        //----

        if ((collider.gameObject.tag == "Target_test" 
            || collider.gameObject.tag == "Player"
            || collider.gameObject.tag == "Zombie"
            ) && collider.gameObject.transform != myParent) 
            //если попал в объект, которому можно нанести урон, но не в своего родителя
        {
            if (collider.gameObject.GetComponent<TargetTest>() != null)
                collider.gameObject.GetComponent<TargetTest>().Hurt(myDamage);
            if (collider.gameObject.GetComponent<PlayerINfo>() != null)
                collider.gameObject.GetComponent<PlayerINfo>().Hurt(myDamage);
            if (collider.gameObject.GetComponent<Zombie>() != null)
                collider.gameObject.GetComponent<Zombie>().Hurt(myDamage);
            coll = collider;
            OnExplode();
            destroyObject();
        }
        else if (collider.gameObject.tag != "isWeapon"
                && collider.gameObject.tag != "weapon1_shoot"
                && collider.gameObject.tag != "weapon2_shoot"
                && collider.gameObject.tag != "health"
                && collider.gameObject.tag != "ladder"
                && collider.gameObject.tag != "fence"
                && collider.gameObject.tag != "fence_2"
                && collider.gameObject.tag != "Untagged"
                && collider.gameObject.tag != "turret"
                && collider.gameObject.transform != myParent)
        {//если указанный тег или родитель - снаряд летит мимо. иначе взрывается
            OnExplode();
            Destroy(gameObject);
        }
    }
    void OnExplode()//Бабах!!!
    {
        if (nameWeapon == "flare-gun")//если это ракета - поражаем цели в некотором радиусе
        {
            flareGunInizialize();
        }
        if(myExplosion != null)
            Instantiate(myExplosion, transform.position, transform.rotation);
    }
    void destroyObject()
    {
        if (nameWeapon == "flare-gun")//ракете нужна некоторая задержка перед уничтожением, чтобы обнаружить остальные цели
        {
            this.renderer.enabled = false;
            Destroy(gameObject, 0.05f);
        }
        else Destroy(gameObject);
    }

    void flareGunInizialize()
    {
        explosion = true;
        BoxCollider2D BC;
        BC = this.GetComponent<BoxCollider2D>();
        BC.isTrigger = false;
        BC.size = new Vector2(18f, 3f);
        BC.isTrigger = true;
    }
    void flareGunAttack(Collider2D Collider)
    {
        int myDamageGun = 1;
        if (Collider.gameObject.GetComponent<TargetTest>() != null)
            Collider.gameObject.GetComponent<TargetTest>().Hurt(myDamageGun);
        if (Collider.gameObject.GetComponent<PlayerINfo>() != null)
            Collider.gameObject.GetComponent<PlayerINfo>().Hurt(myDamageGun);
        if (Collider.gameObject.GetComponent<Zombie>() != null)
            Collider.gameObject.GetComponent<Zombie>().Hurt(myDamageGun);
        if (Collider.gameObject.GetComponent<fence>() != null)
            Collider.gameObject.GetComponent<fence>().Hurt(myDamageGun);

    }



}
