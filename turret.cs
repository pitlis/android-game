﻿using UnityEngine;
using System.Collections;

public class turret : MonoBehaviour {

    public GameObject weaponActive; // оружие
    public int direction = 1; //текущее направление player: -1 - лево, 1 - право.

    public float time;//скорость стрельбы
    public float myTimer;

    private RaycastHit2D hit;
    public float distance;

    public bool IamShooting = false;
    GameObject aim;

	void Start () {
	}

    void FixedUpdate()
    {
        if (myTimer > 0)
        {
            myTimer -= Time.deltaTime;
        }
        else
        {
            myTimer = time;
            hit = Physics2D.Raycast(new Vector2(this.transform.position.x + 0.1f, this.transform.position.y - 0.8f), new Vector2(1, 0), distance, 5120);
           
            if (hit.collider != null && hit.collider.tag == "Zombie")
            {
                if (IamShooting && aim != null && hit.collider.gameObject == aim)
                {
                    shoot();
                    return;
                }

                if (!IamShooting)
                {
                    aim = hit.collider.gameObject;
                    IamShooting = true;
                    if (direction == 1)
                        shoot();
                    else
                    {
                        Flip();
                        shoot();
                    }
                    return;
                }
            }

            hit = Physics2D.Raycast(new Vector2(this.transform.position.x - 0.1f, this.transform.position.y - 0.8f), new Vector2(-1, 0), distance, 5120);
            if (hit.collider != null && hit.collider.tag == "Zombie")
            {
                if (IamShooting && aim != null && hit.collider.gameObject == aim)
                {
                    shoot();
                    return;
                }
                if (!IamShooting)
                {
                    aim = hit.collider.gameObject;
                    IamShooting = true;
                    if (direction == -1)
                        shoot();
                    else
                    {
                        Flip();
                        shoot();
                    }
                    return;
                }
                else
                {
                    IamShooting = false;
                    aim = null;
                }
                return;
            }
            //если зомби нет ни слева, ни справа
            IamShooting = false;
            aim = null;
        }
	}

    /*
    void OnDrawGizmos() // В редакторе проверяем правильность данных полученных от ray
    {
        Gizmos.color = Color.red; // Задаем цвет линии  
        Gizmos.DrawLine(new Vector2(this.transform.position.x + 1f, this.transform.position.y-0.5f), hit.point); //Рисуем саму линию в редакторе
    }
    */

    void shoot()
    {
            weaponActive.GetComponent<Weapon1>().shoot(direction);
    }


    void Flip() //Поворот
    {
        Vector3 theScale = weaponActive.transform.localScale;
        theScale.x *= -1;
        weaponActive.transform.localScale = theScale;
        direction *= -1;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Zombie" || collider.gameObject.tag == "Scanner")
            Destroy(gameObject);//уничтожается при столкновении с зомби
    }

}
