﻿using UnityEngine;
using System.Collections;

public class ScannerForZombie : MonoBehaviour {

    //Скрипт вешается на коллайдер-триггер для обнаружения других зомби
    //Пришлось сделать отдельным объектом, т.к. нужно отключать box collider, а 
    //если на объекте висят несколько - отключить конкретный не получается.

    Zombie parent;

    public int coll = 0;

    void Start()
    {
        parent = this.transform.parent.GetComponent<Zombie>();
    }

    void FixedUpdate()
    {
        if (coll > 0)
            StartCoroutine(stabilic());
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        //если сталкиваюсь с другим зомби (иду в противоположном направлении)
        if (collider.gameObject.tag == "Zombie" && collider.gameObject.GetComponent<Zombie>().direction != parent.direction)
        {
            coll++;
            parent.collider2D.isTrigger = true;
        }
    }


    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Zombie" && collider.gameObject.GetComponent<Zombie>().direction != parent.direction)
        {
            if (coll >= 1) coll--;
            if (coll <= 0)
                parent.collider2D.isTrigger = false;
        }
    }

    private IEnumerator stabilic()
    {
        yield return new WaitForSeconds(1f);
        coll = 0;
        parent.collider2D.isTrigger = false;
        yield return new WaitForSeconds(1f);
        parent.collider2D.isTrigger = true;
        yield break;
    }
}
