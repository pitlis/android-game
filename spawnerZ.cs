﻿using UnityEngine;
using System.Collections;

public class spawnerZ : MonoBehaviour {

    public bool Active = false;

    public System.Collections.Generic.List<GameObject> myObjects;//список объектов, которые может спавнить. Заполняется перетаскиванием префабов в редакторе
    private GameObject myObject;

    public float time;//время до появления нового объекта
    public float myTimer;

    public int maxZombies;//общее кол-во зомби, которое может создать спавнер
    public int ZombieCount;

    public bool pause = false;

    GameSession GS;
    void Start()
    {
        myTimer = time;
        ZombieCount = 0;

        GS = GameObject.Find("GameSession").GetComponent<GameSession>();
    }

    void Update()
    {
        if (myTimer > 0)
        {
            myTimer -= Time.deltaTime;
        }
        else
        {
            myTimer = time;
            if (ZombieCount < maxZombies && !pause) 
                spawn();
        }

    }

    void spawn()
    {
        GS.CountZombiesAtMap++;
        ZombieCount++;
        int random = (int)Random.Range((float)0, (float)myObjects.Count);
        myObject = (GameObject)Instantiate(myObjects[random]);
        myObject.transform.position = this.transform.position;
    }
}
