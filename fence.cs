﻿using UnityEngine;
using System.Collections;

public class fence : MonoBehaviour {

    //Нельзя чтобы в коллайдеры препятствия попадали какие-то статические объекты (спавнеры например)!!!
    //это не фича, это баг

    public Sprite myButtomTexture;

    public float HP;
    public float HPmax = 30f;
    
    public float timeRepair;//задержка ремонта
    private float myTimerRepair;
    public bool isActive = true;
    public int direction;//направление стены: -1 - спавнер слева, 1 - спавнер  справа от стены

    void Start()
    {
        myTimerRepair = 0;
        HP = HPmax;
    }

	void Update () 
    {
        if (myTimerRepair > 0)
        {
            myTimerRepair -= Time.deltaTime;
        }

        if (HP >= 20)
            this.renderer.material.color = Color.green;
        else if (HP >= 10)
            this.renderer.material.color = Color.yellow;
        else if (HP > 0)
            this.renderer.material.color = Color.red;

        if (HP <= 0)
        {
            this.renderer.material.color = Color.black;
            isActive = false;
            GetComponent<BoxCollider2D>().isTrigger = true;
        }
	}

    public void Hurt(int damage)
    {
        HP -= damage;
    }

    public void repair()
    {
        if (myTimerRepair <= 0)
        {
            myTimerRepair = timeRepair;
            if (HP < HPmax)
                HP += 3;
            if (!isActive)
            {
                isActive = true;
                GetComponent<BoxCollider2D>().isTrigger = false;
            }
        }
        
    }
}
