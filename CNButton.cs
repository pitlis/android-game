﻿/**
 * modified by @moscoquera
 * 
 * */


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CNButton : CNControl
{
    
    // Editor variables
    //public float pixelToUnits = 1000f;

    public int myName; //название, чтобы отличать одинаковые кнопки

    // Script-only public variables
    public event FingerLiftedEventHandler FingerLiftedEvent;
    public event FingerTouchedEventHandler FingerTouchedEvent;
    
    /**
     * Private instance variables
     */
    // Button object
    private GameObject Button;
	private GameObject AltButton;

	
	// Some transform cache variables
    private Transform ButtonTransform;
    
	private bool canSwap=true;

	// Use this for initialization
    void Awake()
    {
        CurrentCamera = transform.parent.camera;

        transformCache = transform;
        Button = transformCache.FindChild("Button").gameObject;
		AltButton = transformCache.FindChild("AlternateButton").gameObject;
		AltButton.renderer.enabled=false;
		ButtonTransform = Button.transform;
        
        InitialCalculations();

#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8 || UNITY_BLACKBERRY
        CurrentInputHandler = TouchInputHandler;
#endif
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE
        // gameObject.SetActive(false);
        CurrentInputHandler = MouseInputHandler;
#endif
        if (remoteTesting)
            CurrentInputHandler = TouchInputHandler;
    }

    void Update()
    {
        // Automatically call proper input handler
        CurrentInputHandler();
    }

    /** Our touch Input handler
     * Most of the work is done in this function
     */
    void TouchInputHandler()
    {
        // Current touch count
        int touchCount = Input.touchCount;
        // If we're not yet tweaking, we should check
        // whether any touch lands on our BoxCollider or not
        if (!isTweaking)
        {
            for (int i = 0; i < touchCount; i++)
            {
                // Get current touch
                Touch touch = Input.GetTouch(i);
                // We check it's phase.
                // If it's not a Begin phase, finger didn't tap the screen
                // it's probably just slided to our TapRect
                // So for the sake of optimization we won't do anything with this touch
                // But if it's a tap, we check if it lands on our TapRect 
                // See TouchOccured function
                if (touch.phase == TouchPhase.Began && TouchOccured(touch.position))
                {
                    // We should store our finger ID 
                    myFingerId = touch.fingerId;
                    // If it's a valid touch, we dispatch our FingerTouchEvent
					//ButtonSwap();
					if (FingerTouchedEvent != null){
						FingerTouchedEvent();
					}
                }
            }
        }
        // We take Touch screen position and convert it to local joystick - relative coordinates
        else
        {
            for (int i = 0; i < touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);
                // For every finger out there, we check if OUR finger has just lifted from the screen
                if (myFingerId == touch.fingerId && touch.phase == TouchPhase.Ended)
                {
                    // And if it does, we reset our Joystick with this function
                    ResetJoystickPosition();
                    // We store our boolean here
                    
                    // And dispatch our FingerLiftedEvent
					//ButtonSwap();
                    if (FingerLiftedEvent != null){

                        FingerLiftedEvent();
					}
                }
            }
            
        }
    }
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE
    // Mouse input handler, nothing really interesting
    // It's pretty straightforward
    void MouseInputHandler()
    {
        if (Input.GetMouseButtonDown(0))
        {
			if (TouchOccured(Input.mousePosition)){
				if (canSwap){
					//ButtonSwap();
					canSwap=false;
				}
				if (FingerTouchedEvent != null){
					FingerTouchedEvent();
				}
			
		}
	}
	if (Input.GetMouseButtonUp(0))
        {
			if (!canSwap){
				//ButtonSwap();
				canSwap=true;
			}
            ResetJoystickPosition();
        }
    }
#endif
    /**
     * Snappings calculation
     * Joystick radius calculation based on specified pixel to units value
     * Initial on-screen placement, relative to the specified camera
     */
    void InitialCalculations()
    {
        halfScreenHeight = CurrentCamera.orthographicSize;
        halfScreenWidth = halfScreenHeight * CurrentCamera.aspect;
		screenHeight = halfScreenHeight * 2f;
        screenWidth = halfScreenWidth * 2f;
		halfPoint = new Vector3(halfScreenWidth,halfScreenHeight,0);
        snapPosition = new Vector3();
        cleanPosition = new Vector3(-halfScreenWidth, -halfScreenHeight);
        
        switch (placementSnap)
        {
            // We do nothing, it's a default position
            case PlacementSnap.leftBottom:
                snapPosition.x = -halfScreenWidth + tapZone.width / 2f - tapZone.x;
                snapPosition.y = -halfScreenHeight + tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = 0f;
                tapZone.y = 0f;
                break;
            // We swap Y component
            case PlacementSnap.leftTop:
                snapPosition.x = -halfScreenWidth + tapZone.width / 2f - tapZone.x;
                snapPosition.y = halfScreenHeight - tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = 0f;
                tapZone.y = screenHeight - tapZone.height;
                break;
            // We swap X component
            case PlacementSnap.rightBottom:
                snapPosition.x = halfScreenWidth - tapZone.width / 2f - tapZone.x;
                snapPosition.y = -halfScreenHeight + tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = screenWidth - tapZone.width;
                tapZone.y = 0f;
                break;
            // We swap both X and Y component
            case PlacementSnap.rightTop:
                snapPosition.x = halfScreenWidth - tapZone.width / 2f - tapZone.x;
                snapPosition.y = halfScreenHeight - tapZone.height / 2f - tapZone.y;

                // Tap zone change so we can utilize Rect's .Contains() method
                tapZone.x = screenWidth - tapZone.width;
                tapZone.y = screenHeight - tapZone.height;
                break;
			case PlacementSnap.Fixed:
				snapPosition.x = (tapZone.x/fixedStepSize.x)*halfScreenWidth;
				snapPosition.y = (tapZone.y / fixedStepSize.y)*halfScreenHeight;
				
				// Tap zone change so we can utilize Rect's .Contains() method
			tapZone.x = halfScreenWidth+((tapZone.x/fixedStepSize.x)*halfScreenWidth) - (tapZone.width/2);
			tapZone.y = halfScreenHeight+((tapZone.y / fixedStepSize.y)*halfScreenHeight) - (tapZone.height/2);
			break;
				
        }
        transformCache.localPosition = snapPosition;

        //disabled by @moscoquera
		//ButtonRadius = ButtonSpriteRenderer.bounds.extents.x;

        
    }

    /**
     * Touch or mouse click occured
     * Store initial local position
     * Vector3 touchPosition is in Screen coordinates
     * 
     * Returns true if finger found
     * Returns fals if not
     */
    bool TouchOccured(Vector3 touchPosition)
    {
        
        ScreenPointToRelativeFrustumPoint(touchPosition);
		if ((DetectionType==TouchDetectionType.area && tapZone.Contains(screenPointInUnits))
		    || (DetectionType==TouchDetectionType.radius && ((screenPointInUnits-halfPoint)-snapPosition).sqrMagnitude <= joystickMovementRadius * joystickMovementRadius))
		    {
            isTweaking = true;
            //invokeTouchPosition = screenPointInUnits;
            //transformCache.localPosition = cleanPosition;
            //ButtonTransform.localPosition = invokeTouchPosition;
         
            return true;
        }

        /** OBSOLETE
        Ray screenRay = CurrentCamera.ScreenPointToRay(touchPosition);
        RaycastHit hit;
        if (Physics.Raycast(screenRay, out hit, distanceToCamera * 2f))
        {
            if (hit.collider == collider)
            {
                isTweaking = true;
                invokeTouchPosition = transform.InverseTransformPoint(hit.point);
                invokeTouchPosition.z = 0f;
                Button.transform.localPosition = invokeTouchPosition;
                joystick.transform.localPosition = invokeTouchPosition;
                return true;
            }
        }* */
        return false;
    }


    /**
     * Resetting BOTH joystick sprites to their initial position
     */
    void ResetJoystickPosition()
    {
        isTweaking = false;
        transformCache.localPosition = snapPosition;
        ButtonTransform.localPosition = Vector3.zero;
        myFingerId = -1;
    }

    /**
     * We need to convert our touch or mouse position to our local joystick position
     */
    void ScreenPointToRelativeFrustumPoint(Vector3 point)
    {
        // Percentage
        float screenPointXPercent = point.x / Screen.width;
        float screenPointYPercent = point.y / Screen.height;

        screenPointInUnits.x = screenPointXPercent * screenWidth;
        screenPointInUnits.y = screenPointYPercent * screenHeight;
        screenPointInUnits.z = 0f;
        /** OBSOLETE
        // Dirty magic again, finding super - local coordinates of the touch position
        joystickRelativePosition.x = screenPointXPercent * frustumWidth;
        joystickRelativePosition.y = screenPointYPercent * frustumHeight;
        joystickRelativePosition -= relativeExtentSummand;
        joystickRelativePosition.z = 0f;
         * */
    }

    // Sometimes when user lifts his finger, current touch index changes.
    // To keep track of our finger, we need to know which finger has the user lifted
    int FindMyFingerId()
    {
        int touchCount = Input.touchCount;
        for (int i = 0; i < touchCount; i++)
        {
            if (Input.GetTouch(i).fingerId == myFingerId)
            {
                // We return current Touch index if it's our finger
                return i;
            }
        }
        // And we return -1 if there's no such finger
        // Usually this happend after user lifts the finger which he touched first
        return -1;
    }

    

	void ButtonSwap(){
		GameObject tmp = Button;
		Button=AltButton;
		Button.transform.position = tmp.transform.position;
		Button.renderer.enabled=true;
		AltButton=tmp;
		AltButton.renderer.enabled=false;

	}
    public void ChangeTexture(Sprite sprite)
    {
        SpriteRenderer SR = GetComponentInChildren<SpriteRenderer>();
        SR.sprite = sprite;
        SR.transform.localScale = new Vector3(3f, 3f, 1f);

    }



}
