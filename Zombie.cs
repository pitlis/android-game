﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour {

    public float HP = 20f;
    public float playerSpeed;  //скорость
    public int myDamage; //урон от атаки

    public int direction = -1; //текущее направление player: -1 - лево, 1 - право.

    private RaycastHit2D hitDirection;
    private RaycastHit2D hit;
    public float distance;

    public bool Player = false;//если вижу игрока
    public bool PlayerLost = false;//если потерял игрока, но помню о нем

    //ТАЙМЕРЫ
    public float timeMemory;//время памяти об игроке
    private float myTimerMemory;

    public float timeLadder;//время между переходами по лестницам
    private float myTimerLadder;

    public float timeAttack;//скорость атаки
    public float myTimerAttack;
    //----

    private Collider2D Fenc = null;//препятствие, возле которого находится зомби

    private GameSession GS;

    public int CanNotGo = 0;//сторона, в которую в данный момент запрещено двигаться. 0 - ничего не запрещено, -1 - лево, 1 - право.
    public float Xposition;
    public bool SavePosition = false;


    void Start()
    {
        myTimerMemory = timeMemory;
        GS = GameObject.Find("GameSession").GetComponent<GameSession>();
    }
    void FixedUpdate()
    {
        if (myTimerLadder > 0)//задержка между переходами по лестницам
        {
            myTimerLadder -= Time.deltaTime;
        }

        hitDirection = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y), new Vector2(direction, 0), distance, 6144);
        hit = Physics2D.Raycast(new Vector2(this.transform.position.x, this.transform.position.y), new Vector2(-1 * direction, 0), distance, 6144);


        if (hitDirection.collider != null)
        {
            if (hitDirection.collider.tag == "wall" && hitDirection.fraction <= 0.1f)//если уперся в стену - разворачиваюсь
                Flip();
            if (hitDirection.collider.tag == "Player" && hitDirection.fraction <= 0.1f)//подошел на близкое расстояние к игроку - атакую
                attack();
        }
        if (Fenc != null) //рядом с препятствием - атакую
            attackFence(Fenc);


        //Увидел игрока на расстоянии
        if (seePlayer() == 1)//увидел игрока впереди
        {
            PlayerLost = false;
            Player = true;
            myTimerMemory = timeMemory;
        }

        if (seePlayer() == -1)//увидел игрока позади
        {
            PlayerLost = false;
            Player = true;
            Flip();
            myTimerMemory = timeMemory;
        }
        //----

        //если потерял игрока - забываем о нем через какое-то время
        if ((Player || PlayerLost) && seePlayer() == 0)
        {
            PlayerLost = true;
            Player = false;
            if (myTimerMemory > 0)
            {
                myTimerMemory -= Time.deltaTime;
            }
            else 
            {
                PlayerLost = false;
                myTimerMemory = timeMemory;
            }
        }
        //----

        run(direction);

        //Чтобы зомби не проталкивали друг друга сквозь стену
        if (CanNotGo != 0 && !SavePosition)
        {
            Xposition = this.transform.position.x;
            SavePosition = true;
        }
        if (CanNotGo == 0 && SavePosition)
        {
            SavePosition = false;
        }
        if (SavePosition)
        {
            if (CanNotGo == 1 && this.transform.position.x > Xposition)
                transform.position = new Vector2(Xposition, this.transform.position.y);
            if (CanNotGo == -1 && this.transform.position.x < Xposition)
                transform.position = new Vector2(Xposition, this.transform.position.y);
        }
    }

    private int seePlayer()//проверка на видимость игрока на расстоянии. 1 - игрок впереди, -1 - игрок позади, 0 - не вижу игрока.
    {
        if (hitDirection.collider != null && hitDirection.collider.tag == "Player")
            return 1;
        if (hit.collider != null && hit.collider.tag == "Player")
            return -1;
        return 0;
    }
    void run(float X)
    {
        if (CanNotGo != direction)
        {
            float transH = X * playerSpeed * Time.deltaTime;
            transform.Translate(new Vector2(transH, 0));
        }
    }
    void Flip() //Поворот
    {
        direction *= -1;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    public void Hurt(int damage)
    {
        HP -= damage;
        if (HP <= 0)
        {
            Kill();
        }
    }
    private void Kill()
    {
        GS.KillZombie();
        Destroy(gameObject);
    }

    public void attack()
    {
        if (myTimerAttack > 0)
        {
            myTimerAttack -= Time.deltaTime;
        }
        else
        {
            myTimerAttack = timeAttack;
            hitDirection.collider.gameObject.GetComponent<PlayerINfo>().Hurt(myDamage);
        }
        run(-direction);//временно, чтобы компенсировать движение во время атаки игрока
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "ladder")//перехожу по лестнице, если прошло время после предыдущего перехода
        {
            if (myTimerLadder <= 0)
                ladder(collider);
        }
        if (collider.gameObject.tag == "fence" && collider.gameObject.name == "toSpawner")//если возле препятствия - атакую его каждый FixedUpdate
        {
            Fenc = collider;
        }
        if (collider.gameObject.tag == "fence_2")//если возле препятствия - атакую его каждый FixedUpdate
        {
            Fenc = collider;
            return;
        }
        if (collider.gameObject.tag == "fence" && collider.gameObject.name == "toPlayer" && 
            collider.gameObject.transform.parent.gameObject.GetComponent<fence>().direction == direction)//подошел к препятствию со стороны игрока - разворачиваюсь
        {
            Flip();
        }
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "fence")//если больше не нахожусь возле препятствия
        {
            Fenc = null;
        }
        if (collider.gameObject.tag == "fence_2")//если больше не нахожусь возле препятствия
        {
            Fenc = null;
        }
    }

    private void attackFence(Collider2D collider)
    {
        if (myTimerAttack > 0)
        {
            myTimerAttack -= Time.deltaTime;
        }
        else
        {
            myTimerAttack = timeAttack;
            if (collider.gameObject.tag == "fence")
                if (collider.gameObject.transform.parent.gameObject.GetComponent<fence>().isActive)
                    collider.gameObject.transform.parent.gameObject.GetComponent<fence>().Hurt(myDamage);
            if (collider.gameObject.tag == "fence_2" && collider.gameObject.name == "fence2")
            {
                if (collider.gameObject.GetComponent<fence_2>().isActive)
                    if (collider.gameObject.GetComponent<fence_2>().Hurt(myDamage))//если стена уничтожена
                        CanNotGo = 0;
            }
        }
    }
    
    private void ladder(Collider2D collider)
    {
        myTimerLadder = timeLadder;

        if (Player) return; //вижу игрока - не перехожу

        if (!Player && PlayerLost)//если не вижу игрока, но помню о нем - перехожу
        {
            collider.gameObject.GetComponent<ladder>().teleport(this.collider2D);
        }

        if (!Player && !PlayerLost)//не вижу, не помню - перехожу с вероятностью 30%
        {
            int r = (int)Random.Range((float)0, (float)3);
            if (r == 1)
                collider.gameObject.GetComponent<ladder>().teleport(this.collider2D);
        }
    }

}
